# F2CSV
このアプリケーションは、OpenCVのFacemarkのLBFモデルにもとづき、
user.home/.dod/csv/に、user.home/.dod/images/の中身の顔画像をを全てを、
特徴点の座標として保存する。(CSV形式)

## 使い方
まずはじめに下記のディレクトリを作成してください。
(配布されているjarファイルでは全自動で、生成されます。)
```
  user.home/ - .dod/ -- models/
                    |- images/
                    |- csv/
```
生成後にimages/に顔画像を挿れてください。  

次に、下記のコマンドを実行すれば完了です。
```
  $ gradle build
  $ gradle run
```